/**
 * Created by Jaime on 30/05/2019.
 */

$("#table-users").DataTable({
    proccesing: true,
    serverSide: true,
    language: {
        "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
    },
    responsive:true,
    ajax:'list-users',
    columns:[
        {data: 'id', name:'id'},
        {data: 'name', name: 'name'},
        {data: 'rol', name: 'rol'},
        {data: 'email', name: 'email'},
        {data: 'id', render: function (data, type, row, meta) {
            var status = row.status == 0 ? "Activar": "Desactivar";
            var class_status = row.status == 0 ? "btn btn-danger":"btn btn-success";
            var icon_status = row.status == 0 ? "ion-ios-thumbs-down": "ion-ios-thumbs-up";

            return '<a href="#" class="btn btn-primary edit-user" data-id="'+data+'" data-toggle="tooltip" data-placement="top" title="Editar usuario"><span class="ion ion-md-create"></span></a>  ' +
                '<a  href="#" class="btn btn-info permits-user" data-id="'+data+'" data-toggle="tooltip" data-placement="top" title="Permisos de usuario"><span class="ion-md-list"></span></a>  ' +
                '<a href="#" class="'+class_status+' change-status-user" data-id="'+data+'" data-toggle="tooltip" data-placement="top" title="'+status+'"><span class="'+icon_status+'"></span></a>';
        }}
    ],
    columnDefs:[
        {className: "text-center", targets:[4]}
    ]
});