@extends('layouts.app')
@section('content')
    <div class="container">
        <table class="table table-bordered table-striped" id="table-users">
            <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Email</th>
                <th>Opciones</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    @endsection