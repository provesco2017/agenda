<?php

namespace App\Http\Controllers;

use App\Http\Repositories\UsersRepo;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class UsersController extends Controller
{
    /**
     * @var UsersRepo
     */
    private $usersRepo;

    public function __construct(UsersRepo $usersRepo)
    {
        $this->usersRepo = $usersRepo;
    }

    public function index(){
        $users = $this->usersRepo->all();
        return view('users.index',compact('users'));
    }

    public function listUsersAjax(){
        $users = $this->usersRepo->all();

    }
}
