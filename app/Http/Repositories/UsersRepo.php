<?php
/**
 * Created by PhpStorm.
 * User: Jaime
 * Date: 28/05/2019
 * Time: 05:55 PM
 */
namespace App\Http\Repositories;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;
class UsersRepo extends BaseRepo {
    public function getModel()
    {
        return new User();
    }
};