<?php
/**
 * Created by PhpStorm.
 * User: Jaime
 * Date: 28/05/2019
 * Time: 05:31 PM
 */

Route::get('users','UsersController@index')->name('users');
Route::get('list-users','UsersController@listUsersAjax')->name('list-users');